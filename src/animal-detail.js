const animals = [
	{
		id: 1,
		name: 'Black Rhino',
		animalImg: 'images/black-rhino.png',
		mapImg: './maps/black-rhino.PNG',
	},
];

export default animals;
