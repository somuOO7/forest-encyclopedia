import React from 'react';

function Card(props) {
	return (
		<div className='col-lg-3 col-md-6'>
			<div className='row animal-box'>
				<div className='col-6 text-box'>
					<img src={props.map} alt='' />
					<h3>{props.name}</h3>
				</div>
				<div className='col-6 col img-box'>
					<img src={props.img} alt='' />
				</div>
			</div>
		</div>
	);
}

export default Card;
