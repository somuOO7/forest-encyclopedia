import React from 'react';
import Card from './Card.jsx';
import animal from '../animal-detail';

function createCard(animal) {
	return (
		<Card
			key={animal.id}
			name={animal.name}
			img={animal.animalImg}
			map={animal.mapImg}
		/>
	);
}

function App() {
	return (
		<div className='container'>
			<div className='row'>{animal.map(createCard)}</div>
		</div>
	);
}

export default App;
